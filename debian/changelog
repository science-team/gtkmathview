gtkmathview (0.8.0-15) UNRELEASED; urgency=medium

  * VCS moved to salsa
  * Fix the FTBFS with gcc 7. Thanks to Juhani Numminen
    for the patch (Closes: #853436)
  * Standards-Version: 4.1.3

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 16 Apr 2018 10:29:00 +0200

gtkmathview (0.8.0-14) unstable; urgency=medium

  * Fix the FTBFS with gcc 6. Thanks to Gert Wollny
    (Closes: #811682)
  * Standards-Version updated to 3.9.8

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 26 Jun 2016 19:02:03 +0200

gtkmathview (0.8.0-13) unstable; urgency=medium

  * Reimport changes from the previous NMUs
    My bad, sorry.

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 31 Aug 2015 10:11:55 +0200

gtkmathview (0.8.0-11) unstable; urgency=medium

  [ Michael Terry ]
  * debian/patches/fix-cpp-headers.patch:
   - Pull includes outside of an 'extern "C"' section, fixing FTBFS
   (Closes: #785485)

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 17 Aug 2015 19:22:13 +0200

gtkmathview (0.8.0-10+nmu2) unstable; urgency=low

  * Non-maintainer upload.
  * Build with dh-autoreconf for new architecture support (Closes: 757134)
  * Link with -lm (for ppc64el)

 -- Wookey <wookey@debian.org>  Tue, 21 Oct 2014 01:59:31 +0000

gtkmathview (0.8.0-10+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * Remove libt1-dev dependency from libgtkmathview-dev (closes: #638761).

 -- Michael Gilbert <mgilbert@debian.org>  Fri, 28 Mar 2014 21:41:51 +0000

gtkmathview (0.8.0-10) unstable; urgency=medium

  * Remove t1lib dependency (Closes: #638761)
  * Bump Standards-Version to 3.9.5

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 05 Mar 2014 18:33:00 +0100

gtkmathview (0.8.0-9) unstable; urgency=low

  * libpangox-1.0-dev was not be declared as an explicit dep (Closes: #713213)
  * Bump Standards-Version to 3.9.4
  * Lintian warning package-name-doesnt-match-sonames overrided
  * Switch to dpkg-source 3.0 (quilt) format

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 20 Aug 2013 18:42:03 +0200

gtkmathview (0.8.0-8) unstable; urgency=low

  * Team upload
  * Fix build failure with GCC 4.7, thanks Matthias Klose (Closes: #667195)

 -- Dmitrijs Ledkovs <xnox@debian.org>  Sun, 27 May 2012 13:42:55 +0100

gtkmathview (0.8.0-7) unstable; urgency=low

  * Update the Vcs URLs
  * Wipe out dependency_libs from .la files. Thanks Steve Langasek
   (Closes: #620612)
  * Really fix the FTBFS. Thanks Steve Langasek. (Closes: #618328)

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 06 May 2011 19:28:19 +0200

gtkmathview (0.8.0-6) unstable; urgency=low

  * Fixes the FTBFS with binutils-gold (Closes: #554753)
  * bump Standards-Version to 3.9.1

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 13 Mar 2011 19:00:15 +0100

gtkmathview (0.8.0-5) unstable; urgency=low

  * Adopt this package in the Debian Science context (Closes: #579735)
  * bump Standards-Version to 3.8.4 (no changes needed)

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 10 May 2010 10:22:00 +0200

gtkmathview (0.8.0-4) unstable; urgency=low

  * move packaging to Git, updated Vcs-* fields accordingly
  * switch from dpatch to quilt for patch management; in the switch fix
    non-applicable patches which induced FTBFS (Closes: #560552)
  * build with standard gcc/g++ (Closes: #533808)
  * fix build against gcc/g++ 4.4; add new patch
    patches/0004-gcc-4.4-build-fixes (Closes: #504913)
  * remove empty postinst for libgtkmathview0c2a
  * bump debhelper compatibility level to 7
  * bump Standards-Version to 3.8.3 (no changes needed)

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 11 Dec 2009 13:45:32 +0100

gtkmathview (0.8.0-3) unstable; urgency=low

  * revamp long description in debian/control
  * change license in debian/copyright to LGPL 3 (new upstream license) and
    update upstream author information
  * migrate debian/copyright to the new proposed format (see
    http://wiki.debian.org/Proposals/CopyrightFormat)
  * rebuild enforcing build-dep on g++-4.3 (as on some architectures gcc 4.3
    is not the default), remove the old build-dep on g++-4.1 accordingly
  * add debian/patches/00dpatch.conf pointing to the upstream tarball
    directory
  * debian/patches/
    - rename patches to remove serie number (00series is enough for that)
    - add patch gcc-4.3 to ensure compilation against gcc 4.3
  * debian/control
    - add missing ${shlib:Depends} to libgtkmathview-bin
    - bump build-dep on gmetadom to the latest which export includes suitable
      for compilation against gcc 4.3

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 01 May 2008 23:29:43 +0200

gtkmathview (0.8.0-2) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * update standards-version, no changed needed
  * remove debian/libgtkmathview0c2a.postinst: it was ancient and everything
    it did is now fully implemented by debhelper-generated postinst
  * add debian/libgtkmathview0c2a.preinst and take care in it of removing
    obsolete configuration file /etc/gtkmathview/t1.config (Closes: #454373)

  [ Enrico Tassi ]
  * set CXX=g++-4.1 in debian/rules to force ./configure to force a gcc
    version which does not generate segfaulty code (Closes: #441198)
  * added g++-4.1 to build-depends

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 14 Jan 2008 13:56:42 +0100

gtkmathview (0.8.0-1) unstable; urgency=low

  * new upstream release
    - some cleaning stuff during the build process has been moved from
      distclean to maintainerclean, fixing FTBFS when built more than once in
      a row (closes: #442590) ...
  * ... as a consequence revert explicit force in debian/rules to use "clean"
    as a clean target, "distclean" is just fine
  * debian/control:
    - s/Source-Version/binary:Version/ (deprecated substvar)
    - bump t1lib build dep to the minimum required version with the proper Xpm
      deps (recently NMUed)
  * bump debhelper deps and compatibility level to 5
  * promote Homepage, Vcs-Svn, Vcs-Browser to real debian/control fields

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 29 Oct 2007 10:34:45 +0100

gtkmathview (0.7.8-2) unstable; urgency=low

  * debian/rules
    - invoke upstream 'clean' instead of 'distclean' upon clean, fix a FTBFS
      when building twice in a row (closes: #424392)

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 01 Sep 2007 15:36:29 +0200

gtkmathview (0.7.8-1) unstable; urgency=low

  * new upstream release
    - fixes FTBFS with GCC 4.3 (closes: #417228)
  * injected package sources in the collab-maint repository
  * debian/control
    - added X-Vcs-{Svn/Browser} fields to the source package, pointing to the
      repository URLs
    - added Homepage pseudo-field in the long description
  * debian/watch
    - added watch file
  * debian/patches/
    - removed no longer needed patch 05_t1_config as T1 lib no longer needs to
      be externally configured for gtkmathview (patched debian/*.install files
      accordingly)

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 08 May 2007 13:39:02 +0000

gtkmathview (0.7.7-1) unstable; urgency=low

  * New upstream release (bug fix releases, fixes some issues with computer
    modern fonts and with the munder layout schemata)
  * rebuilding gtkmathview should get rid of the reference to libXcursor.la,
    now gone (closes: #365799)
  * debian/control
    - bumped standards version to 3.7.2, no changes needed

 -- Stefano Zacchiroli <zack@debian.org>  Sat,  5 Aug 2006 15:06:39 +0200

gtkmathview (0.7.5-3) unstable; urgency=low

  * debian/rules
    - removed work around needed to integrate dpatch-edit-patch with cdbs
      (see #284231). Fixes FTBFS (closes: #356101).

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 10 Mar 2006 07:32:53 -0500

gtkmathview (0.7.5-2) unstable; urgency=low

  * debian/control
    - rebuild/renaming due to changed libstdc++ configuration
      (closes: #339180)

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 17 Nov 2005 10:40:00 +0100

gtkmathview (0.7.5-1) unstable; urgency=low

  * New upstream release
    - added API for clearing GtkMathView's internal caches
    - fixed bugs about selection of Computer Modern Fonts
    - glyph tables
    - scale columns in MathML tables
    - patched config.h: fixes FTBFS on arm

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 29 Sep 2005 17:20:27 +0200

gtkmathview (0.7.4-2) unstable; urgency=low

  * debian/control
    - added build-dep on libpopt-dev (Closes: Bug#328060)
    - bumped standards-version
  * debian/copyright
    - updated some info
    - reference GPL shipped in /usr/share/common-licenses/

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 13 Sep 2005 15:31:57 +0200

gtkmathview (0.7.4-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 12 Sep 2005 13:26:33 +0200

gtkmathview (0.7.2-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 31 Jul 2005 15:50:31 +0200

gtkmathview (0.7.1-1) unstable; urgency=low

  * New upstream release
    - better support for gtk fonts
  * debian/*
    - ABI transition for gcc 4
  * debian/control
    - bumped gmetadom dependencies to 0.2.3

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  7 Jul 2005 12:20:49 +0000

gtkmathview (0.7.0-1) unstable; urgency=low

  * New upstream release
    - many new features, including support for SVG output, improved support
      for Computer Modern fonts, high-quality rendering, TrueType fonts, and
      much more

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 13 Jun 2005 12:16:25 +0200

gtkmathview (0.6.5-2) unstable; urgency=low

  * debian/rules
    - uses cdbs
  * debian/control
    - added build-dep on cdbs, bumped debhelper deb accordingly
  * debian/libgtkmathview0.info
    - removed, since info documentation is no longer shipped upstream

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 23 May 2005 09:00:53 +0200

gtkmathview (0.6.5-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 22 Apr 2005 13:29:52 +0200

gtkmathview (0.6.4-1) unstable; urgency=low

  * New upstream release (bugfix release)
  * debian/control
    - bumped build dependency on gtk2.0 to >= 2.4.10, needed by this
      upstream version

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 10 Nov 2004 13:38:44 +0100

gtkmathview (0.6.3-1) unstable; urgency=low

  * New upstream release
  * debian package now uses dpatch for debian specific patches
  * debian/patches/01_conffiles.dpatch
    - moves math-engine-configuration.xml to /etc/gtkmathview
  * debian/control
    - added deps on dpatch
    - bumped standards-version to 3.6.1.1
  * debian/rules
    - added dpatch specific targets and dependencies

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 27 Jul 2004 14:47:03 +0200

gtkmathview (0.6.2-1) unstable; urgency=low

  * New upstream release
    - this is another development release for gtkmathview, which introduces a
      totally redesigned "frontend" for the widget. Starting from this
      version, the frontend is completely decoupled from the formatting engine
      and it is thus much easier to feed gtkmathview with MathML documents
      available by means other than gmetadom. See the announcement on the
      mailing list for more details
  * Got rid of ancient, no longer needed, patches
  * Removed support for postscript exportation
  * Removed support for t1lib

 -- Stefano Zacchiroli <zack@debian.org>  Fri,  7 May 2004 13:31:16 +0200

gtkmathview (0.5.2-3) unstable; urgency=low

  * debian/control
    - depends on newer libt1 (>= 5.0.0)
  * src/T1_FontManager.cc
    - uses newer libt1 method (Closes: Bug#228548)

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 22 Jan 2004 12:20:52 +0100

gtkmathview (0.5.2-2) unstable; urgency=low

  * Declare some previously undeclared extern functions in guiGTK.c
    (Closes: Bug#226528)

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 12 Jan 2004 16:39:33 +0100

gtkmathview (0.5.2-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - bumped standards version

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 12 Nov 2003 16:40:13 +0100

gtkmathview (0.5.1-1) unstable; urgency=low

  * New upstream release (notably: ported to gtk2/glib2)
  * Patched gtkmathview.pc.in with the right dependencies on glib2/gtk2

 -- Stefano Zacchiroli <zack@debian.org>  Tue,  7 Oct 2003 19:33:41 +0200

gtkmathview (0.4.3-3) unstable; urgency=low

  * moved /usr/share/* stuff (.xml configuration files and DTDs) from
    libgtkmathview-bin package to libgtkmathview0 package
    (Closes: #200653)
  * debian/control
    - added Conflicts from libgtkmathview0 to old libgtkmathview-bin

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 11 Jul 2003 16:43:58 +0200

gtkmathview (0.4.3-2) unstable; urgency=low

  * debian/control
    - bumped gmetadom dependencies to >= 0.1.10-2 (to ensure that .pc is
      available)

 -- Stefano Zacchiroli <zack@debian.org>  Tue,  1 Jul 2003 10:26:21 +0200

gtkmathview (0.4.3-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - bumped dependencies on gmetadom to 0.1.9
    - bumped standards-version to 3.5.10
    - added ${misc:Depends}
    - changed section of libgtkmathview-dev to "libdevel"
    - added Build-Dep on pkg-config
  * debian/rules
    - removed DH_COMPAT in favour of debian/compat
    - removed dh_movefiles in favour of dh_install
    - removed dh_undocumented for a no longer necessary manpage
    - temporarly commented out dh_installinfo because
    - cosmetic changes
    - ship pkg-config .pc file

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 18 Jun 2003 18:39:48 +0200

gtkmathview (0.4.1-2) unstable; urgency=low

  * Use distclean in debian/rules clean target (Closes: Bug#178877)
  * Removed some garbage (like already built Makefiles) from .diff.gz

 -- Stefano Zacchiroli <zack@debian.org>  Fri,  7 Mar 2003 13:17:36 +0100

gtkmathview (0.4.1-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 17 Feb 2003 16:25:31 +0100

gtkmathview (0.4.0-1) unstable; urgency=low

  * New upstream release (notably from this version editing support is
    available)
  * Removed debian/ dir from CVS, gtkmathview is no more a debian native
    package
  * Rebuilt against gmetadom 0.1.4 (changed build-depends accordingly)
  * Bugfix: added dependecies on libxml2-dev and libgdome2-cpp-smart-dev for
    libgtkmathview-dev
  * Changed packages description to include mention of editing support
  * Bumped libgdome2-cpp-smart-dev build dependency to 0.1.5
  * Versioned libgdome2-cpp-smart-dev dependency with >= 0.1.5

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 13 Jan 2003 10:55:04 +0100

gtkmathview (0.3.1-1) unstable; urgency=low

  * New upstream release.
  * Bumped DH_COMPAT to 4 in debian/rules and debhelper build-dep in
    debian/control
  * Use DESTDIR to specify installation dir instead of overriding prefix
  * Removed useless binary-indep target from debian/rules
  * Use "make clean" in clean debian/rules target instead of
    "make distclean"
  * Removed full stop at the end of description synopsis to make lintian
    happy
  * Bumped Standards-Version to 3.5.8

 -- Stefano Zacchiroli <zack@debian.org>  Mon,  9 Dec 2002 12:50:46 +0100

gtkmathview (0.3.0-4) unstable; urgency=low

  * Fixed some typos that cause errors when compiling on big endian archs.

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 31 Mar 2002 23:53:33 +0200

gtkmathview (0.3.0-3) unstable; urgency=low

  * Added build-dep on libgdome2-dev (Closes: Bug#140402).

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 29 Mar 2002 11:28:53 +0100

gtkmathview (0.3.0-2) unstable; urgency=low

  * Rebuilt against gmetadom 0.0.3-4

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 26 Mar 2002 18:58:35 +0100

gtkmathview (0.3.0-4) unstable; urgency=low

  * Fixed some typos that cause errors when compiling on big endian archs.

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 31 Mar 2002 23:53:33 +0200

gtkmathview (0.3.0-3) unstable; urgency=low

  * Added build-dep on libgdome2-dev (Closes: Bug#140402).

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 29 Mar 2002 11:28:53 +0100

gtkmathview (0.3.0-2) unstable; urgency=low

  * Rebuilt against gmetadom 0.0.3-4

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 26 Mar 2002 18:58:35 +0100

gtkmathview (0.3.0-4) unstable; urgency=low

  * Fixed some typos that cause errors when compiling on big endian archs.

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 31 Mar 2002 23:53:33 +0200

gtkmathview (0.3.0-3) unstable; urgency=low

  * Added build-dep on libgdome2-dev (Closes: Bug#140402).

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 29 Mar 2002 11:28:53 +0100

gtkmathview (0.3.0-2) unstable; urgency=low

  * Rebuilt against gmetadom 0.0.3-4

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 26 Mar 2002 18:58:35 +0100

gtkmathview (0.3.0-4) unstable; urgency=low

  * Fixed some typos that cause errors when compiling on big endian archs.

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 31 Mar 2002 23:53:33 +0200

gtkmathview (0.3.0-3) unstable; urgency=low

  * Added build-dep on libgdome2-dev (Closes: Bug#140402).

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 29 Mar 2002 11:28:53 +0100

gtkmathview (0.3.0-2) unstable; urgency=low

  * Rebuilt against gmetadom 0.0.3-4

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 26 Mar 2002 18:58:35 +0100

gtkmathview (0.3.0-3) unstable; urgency=low

  * Added build-dep on libgdome2-dev (Closes: Bug#140402).

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 29 Mar 2002 11:28:53 +0100

gtkmathview (0.3.0-2) unstable; urgency=low

  * Rebuilt against gmetadom 0.0.3-4

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 26 Mar 2002 18:58:35 +0100

gtkmathview (0.3.0-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 13 Mar 2002 15:59:49 +0100

gtkmathview (0.2.9-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 21 Nov 2001 11:38:42 +0100

gtkmathview (0.2.8-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 14 Nov 2001 08:28:24 +0100

gtkmathview (0.2.7-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Tue,  4 Sep 2001 21:41:18 +0200

gtkmathview (0.2.5-2) unstable; urgency=low

  * upgraded config.sub and config.guess in order to support hppa and other
    architectures (closes: Bug#104694)

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 14 Jul 2001 22:48:17 +0200

gtkmathview (0.2.5-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 22 May 2001 23:35:46 +0200

gtkmathview (0.2.4-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  5 Apr 2001 10:05:16 +0200


